<?php
/**
 * Created by PhpStorm.
 * User: unlph
 * Date: 2017/4/17
 * Time: 10:01
 */

use Aura\Cli\CliFactory;
use Aura\Di\ContainerBuilder;

require_once __DIR__ . '/vendor/autoload.php';

use \Abacaphiliac\PsrLog4Php\LoggerWrapper;

class App{


    public static $di;

    public $psrLogger;

    public $configIni;

    public $rabbitmq;

    public function __construct()
    {
        $this->diBuilder = new ContainerBuilder();
        self::$di = $this->diBuilder->newInstance();
        $this->psrLogger = new LoggerWrapper(\Logger::getLogger('MyLogger'));
        $this->configIni = parse_ini_file( __DIR__ .'/config.ini');

        self::$di->params['\Tools\Rabbitmq']['logger'] = $this->psrLogger;
        self::$di->params['\Tools\Rabbitmq']['config'] = $this->configIni;
        $this->rabbitmq = self::$di->newInstance('\Tools\Rabbitmq');
    }

    public function rabbitmq_sendMsg(){
        $this->rabbitmq -> dump_config();
    }

    public function rabbitmq_consumeMsg(){
        $this->rabbitmq->start_consume();
    }

}

$app = new App();
//print_r(App::$di);
$cli_factory = new CliFactory;
$context = $cli_factory->newContext($GLOBALS);
$options = array(
    'service:'
);

$getopt = $context->getopt($options);
$mainService = $getopt->get('--service');
if($mainService == 'rabbitmq_sendMsg'){
    $app ->rabbitmq_sendMsg();
}
else if($mainService == 'rabbitmq_consumeMsg'){
    $app ->rabbitmq_consumeMsg();
}


